import { Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'

const Header = () => {
  return (
    <View style={styles.container} >
      <Text style={styles.text} >Explore</Text>
      <Image style={styles.image} source={require('../../assets/images/nasa.png')} />
    </View>
  )
}

export default Header

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:"blue",
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    padding:16,
    maxHeight:100
  },
  text:{
    fontSize:20
  },
  image:{
    width:60,
    height:60
  }
})
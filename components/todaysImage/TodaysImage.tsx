import { Button, Image, StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { useRouter } from 'expo-router'

const TodaysImage = (
  {image, title, date}:{image:string, title:string,date:string}
  ) => {
    const router = useRouter();
    // const title="Edwin Hubble Discovers the Universe"
    // const date="2023-10-06"
    // const image="https://apod.nasa.gov/apod/image/2004/HubbleVarOrig_Carnegie_960.jpg"
    const handlePress = ()=>{
      router.setParams({id:"123"})
      router.push('/details')
      
    }
  return (
    <View style={styles.container} >
      <View>
      <Image style={styles.image} source={{uri:image}}  />
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.date}>{date}</Text>
      </View>
      <View style={styles.buttonContainer} >
        <Button onPress={handlePress} title='View' />
      </View>

    </View>
  )
}

export default TodaysImage

const styles = StyleSheet.create({
  container:{
    padding:20,
  },
  image:{
    borderWidth:1,
    borderColor:"white",
    height:200,
    width:"100%",
    borderRadius:10
  },
  buttonContainer:{
    backgroundColor:"red"
  },
  title:{
    fontSize:20
  },
  date:{
    marginTop:15
  }
})
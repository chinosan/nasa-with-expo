import React from 'react'
import { View, Text, ScrollView, StyleSheet} from 'react-native'
import PostImage from './PostImage'

const LatestImages = ({data}:{data:any})=> {
  // console.log("last five images => ",data)
  return (
    <ScrollView style={styles.container} >
      <Text>Las five Imges</Text>
      {data.map( recs =>
        <PostImage key={recs.title} date={recs.date} title={recs.title} link={recs.url} />
        )}
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1
  }
})
export default LatestImages
import React from 'react'
import {View, Text, Button, StyleSheet} from 'react-native'

const PostImage = ({title,date,link}:{title:string,date:string,link:string}) => {
  return (
    <View>
      <Text>{title}</Text>
      <Text>{date}</Text>
      <Text>{link}</Text>
      <View>
        <Button title='View' />
      </View>
    </View>
  )
}
const styles = StyleSheet.create({
  container:{
    backgroundColor:'blue',
    borderRadius:5,
  },
  title:{
    color:"white",
    fontWeight:'bold'
  },
  buttonContainer:{
    justifyContent:'flex-end'
  },
  button:{
    backgroundColor:'none',
    color:"white"
  }
})
export default PostImage
const API_URL = 'https://api.nasa.gov/planetary/'
const API_KEY = '3PXE6EVSsBGbG1FSxMtef2YPAI1F4ZlkH0WseVGG'

const useFetch = async (params?:string) => {
  try {
    const url = `${API_URL}apod?api_key=${API_KEY}${typeof params !== undefined && params?.length > 0
      ? params
      : ''
  }`;
  // console.log("url: ",url)
    const res = await fetch(url)
    const result = await res.json()
    // console.log(result)
    return Promise.resolve(result)
    } catch (error) {
    return Promise.reject(error)
  }
}

export default useFetch
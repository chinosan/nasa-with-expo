import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import Header from '../components/Header/Header'
import useFetch from '../hooks/UseFetch'
import TodaysImage from '../components/todaysImage/TodaysImage'
import { format, sub } from 'date-fns'
import { IApod } from '../types/types'
import LatestImages from '../components/latestImages'

const Home = () => {
  const [todaysImage, setTodaysImage] = useState({})
  const [lastFiveDaysImages, setLastFiveDaysImages] = useState<IApod[]>([])

  useEffect( ()=>{
    const res = async() => {
      try {
        const data = await useFetch()
        // console.log(data)
        setTodaysImage(data)
      } catch (error) {
        console.error(error)
      }
    }
    const loadLast5DaysImages = async () => {
      try {
        const date = new Date()
        const today = format(date, 'yyyy-MM-dd')
        const fiveDaysAgo = format(sub(date,{days:5}) ,'yyyy-MM-dd')

        // console.log(today, fiveDaysAgo)
        const lastFiveDAysResponse = await useFetch(`&start_date=${fiveDaysAgo}&end_date=${today}`)
        // console.log(lastFiveDAysResponse)
        setLastFiveDaysImages(lastFiveDAysResponse)
      } catch (error) {
        console.error(error)
      }
    }
    res().catch(null)
    loadLast5DaysImages().catch(null)
  },[])

  return (
    <View style={{flex:1}} >
      <Header/>
      <TodaysImage 
      image={todaysImage.url} 
      title={todaysImage.title} 
      date={todaysImage.date} 
      />
      <LatestImages data={lastFiveDaysImages} />
    </View>
  )
}

export default Home

const styles = StyleSheet.create({})